package com.ing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ing.dao.OrderDAO;
import com.ing.model.Order;
import com.ing.response.OrderResponse;

/*
 * OrderServiceImpl
 */

@Component
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderDAO orderDAO;
	
	public OrderResponse createOrder(Order orderObj) throws Exception {
		long orderId=orderDAO.saveOrder(orderObj);
		OrderResponse orderResponseObj = new OrderResponse();
		orderResponseObj.setOrderId(orderId);
		
//		caliculateExchangeRate(order.getSell());

		return orderResponseObj;
	}

	private void caliculateExchangeRate(double sell) {

	}

	public List<Order> getAllOrders(Order order) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public List<Order> getAllOrders(Order order) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}*/

}
