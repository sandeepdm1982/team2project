package com.ing.service;

import java.util.List;

import com.ing.model.Order;
import com.ing.response.OrderResponse;

/*
 * OrderService
 */
public interface OrderService {

	public OrderResponse createOrder(Order order) throws Exception;

	public List<Order> getAllOrders(Order order) throws Exception;
}
