package com.ing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ing.response.OrderResponse;
import com.ing.service.OrderService;


@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/saveorder", method = RequestMethod.POST, produces = "application/json",consumes="application/json")	
	public OrderResponse saveOrder(@RequestBody com.ing.model.Order order) throws Exception{
		System.out.println("OrderController.saveOrder() in controller");
		return orderService.createOrder(order);		
	}	

}
