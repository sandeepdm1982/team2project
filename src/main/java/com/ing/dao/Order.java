package com.hcl.samle;

import java.util.Date;

public class Order {
	int userid;
	double sell, buy, rate, fee;
	char from,to,country;
	Date order_time;
	
	
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public double getSell() {
		return sell;
	}
	public void setSell(double sell) {
		this.sell = sell;
	}
	public double getBuy() {
		return buy;
	}
	public void setBuy(double buy) {
		this.buy = buy;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public char getFrom() {
		return from;
	}
	public void setFrom(char from) {
		this.from = from;
	}
	public char getTo() {
		return to;
	}
	public void setTo(char to) {
		this.to = to;
	}
	public char getCountry() {
		return country;
	}
	public void setCountry(char country) {
		this.country = country;
	}
	public Date getOrder_time() {
		return order_time;
	}
	public void setOrder_time(Date order_time) {
		this.order_time = order_time;
	}

}
