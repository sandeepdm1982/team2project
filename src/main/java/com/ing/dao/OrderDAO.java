package com.ing.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.ing.model.Order;

@Component
public class OrderDAO{
	
	JdbcTemplate jdbcTemplate=null;
	private String query;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	
	public long saveOrder(final Order orderObj){
		/*query="insert into orders(userid,from,to,toSell,originCountry,time) values("+orderObj.getUserid()+","+orderObj.getFrom()+","+orderObj.getTo()","+orderObj.getToSell(),","+orderObj.getOriginCountry()+","+orderObj.getTime());
		int status=jdbcTemplate.update(query);*/
		 final PreparedStatementCreator psc = new PreparedStatementCreator() {
		      
		      public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
//		        final PreparedStatement ps = connection.prepareStatement("INSERT INTO `names` (`name`) VALUES (?)",
		    	  final PreparedStatement ps = connection.prepareStatement("INSERT INTO orders(userid,from,to,toSell,originCountry,time) values(?,?,?,?,?,?)",
		            Statement.RETURN_GENERATED_KEYS);
		        ps.setInt(1, orderObj.getUserid());
		        ps.setString(2, orderObj.getFrom());
		        ps.setString(3, orderObj.getTo());
		        ps.setDouble(4, orderObj.getToSell());
		        ps.setString(5, orderObj.getOriginCountry());
//		        ps.setDate(6, orderObj.getTime());
		        return ps;
		      }
		    };

		    // The newly generated key will be saved in this object
		    final KeyHolder holder = new GeneratedKeyHolder();

		    jdbcTemplate.update(psc, holder);

		    final long newNameId = holder.getKey().longValue();
		    return newNameId;
		   
	}
}